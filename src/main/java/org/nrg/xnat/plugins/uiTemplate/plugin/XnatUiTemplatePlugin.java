/*
 * xnat-template: org.nrg.xnat.plugins.template.plugin.XnatUiTemplatePlugin
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.plugins.uiTemplate.plugin;

import org.nrg.framework.annotations.XnatPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.jdbc.core.JdbcTemplate;

@XnatPlugin(value = "uiTemplate", name = "XNAT 1.7 UI Template Plugin")
public class XnatUiTemplatePlugin {

    public XnatUiTemplatePlugin() {
        _log.info("Creating the XnatUiTemplatePlugin configuration class");
    }

    @Bean
    public String uiPluginMessage() {
        return "This comes from deep within the template plugin.";
    }

    @Bean
    public String uiPluginInfo() {
        return "This plugin is a template for adding basic UI features.";
    }

    private static final Logger _log = LoggerFactory.getLogger(XnatUiTemplatePlugin.class);

}
