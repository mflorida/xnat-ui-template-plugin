# Spawner Configuration Files #

The files in this folder will be processed by XNAT's Spawner service and generate JSON that can
be resolved with an XAPI request. 

For example, a `GET` request to the url...

```
/xapi/spawner/resolve/uiTemplate:siteSettings/root
``` 

...would return JSON that's resolved from the `root` element in the file...

```
src/main/resources/META-INF/xnat/spawner/ui-template/site-settings.yaml
```

The presence of a `site-settings.yaml` file will enable the "Administer > Plugin Settings" menu item
which allows navigation to the page where plugins insert tabs for their site-level settings. The presence
of a `project-settings.yaml` works the same way, but for project-level settings.

## Widget Configs

Refer to the `site-settings.yaml` and `project-settings.yaml` files in this directory for examples of how to structure 
your YAML for widget spawning. Certain items need to be present for proper insertion of tabbed content. Here's a code 
snippet briefly describing the 'tabs' widget structure:

```yaml 
root:
    kind: tabs
    groups:
        uiTemplate: XNAT UI Template
    contents:
        info:
            kind: tab
            group: uiTemplate
            label: Administration
            contents:
                sampleText:
                    tag: div.message
                    content: >
                        <b>Congratulations!</b> If you're reading this, XNAT
                        has discovered the site-level settings for your plugin.
```

The key properties for rendering the proper structure are `kind:`, which determines which 'kind' (type) of widget to render, and
`contents:` (PLURAL), which tells the Spawner library where to insert more elements. For some widget types, a `content:` (SINGULAR)
property will insert text or HTML content as-is directly into the element. The `>` character after the `content:` property above is
YAML syntax for inserting a paragraph.

### YAML Syntax

For more info about YAML, 
[please refer to this website for syntax and specs](http://www.yaml.org/spec/1.2/spec.html).
