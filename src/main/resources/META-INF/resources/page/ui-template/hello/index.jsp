<%@ page session="true" contentType="text/html" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pg" tagdir="/WEB-INF/tags/page" %>

<%-- This template is only used when directly accessing a JSP page. --%>
<%-- This index.jsp file can be directly viewed at: --%>
<%-- /page/ui-template/hello/ --%>

<%-- It's a good idea to put your template files in a folder named for your plugin. --%>
<%-- This *should* prevent conflicts with other plugins. --%>


<c:set var="pageName" value="blank" scope="request"/>


<%-- variables for content injection in to 'special' areas of the page --%>
<c:set var="headTop">
    <!-- headTop is inserted just AFTER the OPENING <head> tag -->
    <script>console.log('headTop')</script>
</c:set>


<c:set var="headBottom">
    <!-- headBottom is inserted just BEFORE the CLOSING </head> tag -->
    <script>console.log('headBottom')</script>
</c:set>


<c:set var="bodyTop">
    <!-- bodyTop is inserted just AFTER the OPENING <body> tag -->
    <script>console.log('bodyTop')</script>
</c:set>


<c:set var="bodyBottom">
    <!-- bodyBottom is inserted just BEFORE the CLOSING </body> tag -->
    <script>console.log('bodyBottom')</script>
</c:set>


<pg:wrapper>
    <pg:xnat title="Blank" page="blank" headTop="${headTop}" headBottom="${headBottom}" bodyTop="${bodyTop}" bodyBottom="${bodyBottom}">

        <jsp:include page="content.jsp"/>

    </pg:xnat>
</pg:wrapper>
