<%@ page session="true" contentType="text/html" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pg" tagdir="/WEB-INF/tags/page" %>

<div id="page-wrapper">
    <div class="pad">

        <h1>Hello World!</h1>

        <p>This is a starter JSP template for creating custom pages for your plugin.
            This content will be included into the body of the parent template and displayed
            in the content area of the XNAT UI.</p>

    </div>
</div>
<!-- /#page-wrapper -->
