# Custom Pages

Why are there TWO files needed to display ONE page of content?

The `index.jsp` page is needed for _directly_ accessing the page at a URL like `/page/ui-template/hello/`.

The `content.jsp` page is needed for accessing the page content indirectly at one of the URLs that
pull in the content via `include` or AJAX.

A JSP `include` is done when using the `view` query string parameter from a URL starting with `/page/`, like
`/page/?view=ui-template/hello`.

The content is injected via AJAX when accessing through the JSP `/page/` template using a value in the URL hash,
like `/page/#/ui-template/hello/`, or when using the `Page.vm` Velocity wrapper template: 
`/app/template/Page.vm?view=ui-template/hello`.

The URL hash format is the cleanest and most versatile, and allows loading of ONLY the body content when navigating
between pages, eliminating the need to reload all of the HTML, JavaScript, and CSS files for every page request.
