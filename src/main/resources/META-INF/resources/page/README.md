# Custom Pages

Files placed in this folder can be viewed in a running XNAT by going to one of these special URLs:

```
/app/template/Page.vm?view=ui-template/hello
```
or
```
/page/#/ui-template/hello/
```
or
```
/page/?view=ui-template/hello
```
